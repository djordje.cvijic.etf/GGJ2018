// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:1,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:1,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,imps:False,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:1,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:6,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:False,igpj:True,qofs:1,qpre:4,rntp:5,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:True,fnfb:True,fsmp:False;n:type:ShaderForge.SFN_Final,id:2865,x:34797,y:32977,varname:node_2865,prsc:2|emission-7878-OUT,alpha-9584-OUT;n:type:ShaderForge.SFN_Sin,id:4266,x:32688,y:32848,varname:node_4266,prsc:2|IN-2195-OUT;n:type:ShaderForge.SFN_Slider,id:9958,x:31441,y:32738,ptovrint:False,ptlb:Freq A,ptin:_FreqA,varname:node_9958,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:30.67909,max:100;n:type:ShaderForge.SFN_Multiply,id:8072,x:32314,y:32844,varname:node_8072,prsc:2|A-9958-OUT,B-4624-OUT;n:type:ShaderForge.SFN_Slider,id:4227,x:31441,y:32865,ptovrint:False,ptlb:X A,ptin:_XA,varname:node_4227,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:-0.6365132,max:1;n:type:ShaderForge.SFN_Slider,id:5519,x:31441,y:32988,ptovrint:False,ptlb:Y A,ptin:_YA,varname:node_5519,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:-0.6266626,max:1;n:type:ShaderForge.SFN_Append,id:1135,x:31841,y:32868,varname:node_1135,prsc:2|A-4227-OUT,B-5519-OUT;n:type:ShaderForge.SFN_Slider,id:542,x:31440,y:33510,ptovrint:False,ptlb:X B,ptin:_XB,varname:_sssss_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:-0.1754557,max:1;n:type:ShaderForge.SFN_Slider,id:3209,x:31440,y:33629,ptovrint:False,ptlb:Y B,ptin:_YB,varname:_node_5519_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:0.160981,max:1;n:type:ShaderForge.SFN_Append,id:2472,x:31813,y:33415,varname:node_2472,prsc:2|A-542-OUT,B-3209-OUT;n:type:ShaderForge.SFN_Slider,id:4853,x:31440,y:33388,ptovrint:False,ptlb:Freq B,ptin:_FreqB,varname:_node_9958_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:15.42964,max:100;n:type:ShaderForge.SFN_Multiply,id:1156,x:32333,y:33356,varname:node_1156,prsc:2|A-4853-OUT,B-1115-OUT;n:type:ShaderForge.SFN_Sin,id:4550,x:32717,y:33323,varname:node_4550,prsc:2|IN-7867-OUT;n:type:ShaderForge.SFN_Blend,id:2766,x:33393,y:33089,varname:node_2766,prsc:2,blmd:1,clmp:True|SRC-5783-OUT,DST-7609-OUT;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:9274,x:32963,y:33018,varname:node_9274,prsc:2|IN-4266-OUT,IMIN-7077-OUT,IMAX-5209-OUT,OMIN-2830-OUT,OMAX-9428-OUT;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:9539,x:32963,y:33177,varname:node_9539,prsc:2|IN-4550-OUT,IMIN-7077-OUT,IMAX-5209-OUT,OMIN-2830-OUT,OMAX-9428-OUT;n:type:ShaderForge.SFN_Slider,id:7077,x:32369,y:32397,ptovrint:False,ptlb:Waves input min,ptin:_Wavesinputmin,varname:node_7077,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:-1,max:1;n:type:ShaderForge.SFN_Slider,id:5209,x:32369,y:32509,ptovrint:False,ptlb:Waves input max,ptin:_Wavesinputmax,varname:_node_7077_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:2830,x:32369,y:32615,ptovrint:False,ptlb:Waves output min,ptin:_Wavesoutputmin,varname:_node_7077_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:9428,x:32369,y:32725,ptovrint:False,ptlb:Waves output max,ptin:_Wavesoutputmax,varname:_node_7077_copy_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Clamp01,id:5783,x:33133,y:32969,varname:node_5783,prsc:2|IN-9274-OUT;n:type:ShaderForge.SFN_Clamp01,id:7609,x:33133,y:33177,varname:node_7609,prsc:2|IN-9539-OUT;n:type:ShaderForge.SFN_Slider,id:1005,x:33318,y:32372,ptovrint:False,ptlb:Glow remap in min,ptin:_Glowremapinmin,varname:_inputmin_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.6921345,max:1;n:type:ShaderForge.SFN_Slider,id:6009,x:33318,y:32484,ptovrint:False,ptlb:Glow remap in max,ptin:_Glowremapinmax,varname:_inputmax_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:1166,x:33318,y:32590,ptovrint:False,ptlb:Glow remap out min,ptin:_Glowremapoutmin,varname:_outputmin_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:-1,max:1;n:type:ShaderForge.SFN_Slider,id:5402,x:33318,y:32700,ptovrint:False,ptlb:Glow remap out max,ptin:_Glowremapoutmax,varname:_outputmax_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:1,max:1;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:9584,x:33677,y:33093,varname:node_9584,prsc:2|IN-2766-OUT,IMIN-1005-OUT,IMAX-6009-OUT,OMIN-1166-OUT,OMAX-5402-OUT;n:type:ShaderForge.SFN_Color,id:1724,x:31501,y:32540,ptovrint:False,ptlb:Color A,ptin:_ColorA,varname:node_1724,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.2647059,c2:0.149132,c3:0.06033737,c4:1;n:type:ShaderForge.SFN_Color,id:6848,x:31506,y:33857,ptovrint:False,ptlb:Color B,ptin:_ColorB,varname:node_6848,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.06358133,c2:0.1393778,c3:0.1544118,c4:1;n:type:ShaderForge.SFN_Multiply,id:6234,x:33247,y:32837,varname:node_6234,prsc:2|A-1724-RGB,B-5783-OUT,C-6975-OUT;n:type:ShaderForge.SFN_Multiply,id:1650,x:33252,y:33319,varname:node_1650,prsc:2|A-7609-OUT,B-6848-RGB,C-5840-OUT;n:type:ShaderForge.SFN_Add,id:8559,x:33650,y:33331,varname:node_8559,prsc:2|A-6234-OUT,B-1650-OUT;n:type:ShaderForge.SFN_Append,id:8599,x:33853,y:33077,varname:node_8599,prsc:2|A-9584-OUT,B-9584-OUT,C-9584-OUT;n:type:ShaderForge.SFN_Blend,id:2169,x:34097,y:33166,varname:node_2169,prsc:2,blmd:5,clmp:True|SRC-7716-OUT,DST-8559-OUT;n:type:ShaderForge.SFN_Time,id:7218,x:32003,y:33100,varname:node_7218,prsc:2;n:type:ShaderForge.SFN_Tex2d,id:2724,x:34037,y:33487,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_2724,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_ScreenPos,id:4396,x:31598,y:33201,varname:node_4396,prsc:2,sctp:1;n:type:ShaderForge.SFN_Add,id:7878,x:34485,y:33277,varname:node_7878,prsc:2|A-2169-OUT,B-2724-RGB;n:type:ShaderForge.SFN_Slider,id:5313,x:33806,y:32542,ptovrint:False,ptlb:Damage Zone Mult,ptin:_DamageZoneMult,varname:node_5313,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Distance,id:1115,x:32155,y:33416,varname:node_1115,prsc:2|A-2472-OUT,B-4396-UVOUT;n:type:ShaderForge.SFN_Distance,id:4624,x:32125,y:32928,varname:node_4624,prsc:2|A-1135-OUT,B-4396-UVOUT;n:type:ShaderForge.SFN_Lerp,id:5938,x:34071,y:32853,varname:node_5938,prsc:2|A-6061-RGB,B-3998-RGB,T-8599-OUT;n:type:ShaderForge.SFN_Color,id:6061,x:33822,y:32698,ptovrint:False,ptlb:Damage Zone Tint Start,ptin:_DamageZoneTintStart,varname:node_6061,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5344828,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Color,id:3998,x:33822,y:32872,ptovrint:False,ptlb:Damage Zone Tint End,ptin:_DamageZoneTintEnd,varname:node_3998,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.9914412,c2:1,c3:0.709,c4:1;n:type:ShaderForge.SFN_Multiply,id:5387,x:32314,y:33083,varname:node_5387,prsc:2|A-7218-T,B-9905-OUT;n:type:ShaderForge.SFN_Slider,id:9905,x:31925,y:33248,ptovrint:False,ptlb:Phase Speed Mult,ptin:_PhaseSpeedMult,varname:node_9905,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:0,max:0.0001;n:type:ShaderForge.SFN_Add,id:2195,x:32526,y:32924,varname:node_2195,prsc:2|A-8072-OUT,B-5387-OUT;n:type:ShaderForge.SFN_Add,id:7867,x:32534,y:33307,varname:node_7867,prsc:2|A-5387-OUT,B-1156-OUT;n:type:ShaderForge.SFN_Slider,id:6975,x:31441,y:33095,ptovrint:False,ptlb:Wave Strength A,ptin:_WaveStrengthA,varname:node_6975,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:5840,x:31440,y:33730,ptovrint:False,ptlb:Wave Strength B,ptin:_WaveStrengthB,varname:_WaveStrengthA_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:7716,x:34271,y:32869,varname:node_7716,prsc:2|A-5313-OUT,B-5938-OUT;n:type:ShaderForge.SFN_Slider,id:7220,x:31569,y:33223,ptovrint:False,ptlb:Wave Strength A_copy,ptin:_WaveStrengthA_copy,varname:_WaveStrengthA_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;proporder:9958-4227-5519-542-3209-4853-7077-5209-2830-9428-1005-6009-1166-5402-1724-6848-2724-5313-6061-3998-9905-5840-6975;pass:END;sub:END;*/

Shader "Shader Forge/Radials" {
    Properties {
        _FreqA ("Freq A", Range(0, 100)) = 30.67909
        _XA ("X A", Range(-1, 1)) = -0.6365132
        _YA ("Y A", Range(-1, 1)) = -0.6266626
        _XB ("X B", Range(-1, 1)) = -0.1754557
        _YB ("Y B", Range(-1, 1)) = 0.160981
        _FreqB ("Freq B", Range(0, 100)) = 15.42964
        _Wavesinputmin ("Waves input min", Range(-1, 1)) = -1
        _Wavesinputmax ("Waves input max", Range(-1, 1)) = 1
        _Wavesoutputmin ("Waves output min", Range(0, 1)) = 0
        _Wavesoutputmax ("Waves output max", Range(0, 1)) = 1
        _Glowremapinmin ("Glow remap in min", Range(0, 1)) = 0.6921345
        _Glowremapinmax ("Glow remap in max", Range(0, 1)) = 1
        _Glowremapoutmin ("Glow remap out min", Range(-1, 1)) = -1
        _Glowremapoutmax ("Glow remap out max", Range(-1, 1)) = 1
        _ColorA ("Color A", Color) = (0.2647059,0.149132,0.06033737,1)
        _ColorB ("Color B", Color) = (0.06358133,0.1393778,0.1544118,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _DamageZoneMult ("Damage Zone Mult", Range(0, 1)) = 0
        _DamageZoneTintStart ("Damage Zone Tint Start", Color) = (0.5344828,0,0,1)
        _DamageZoneTintEnd ("Damage Zone Tint End", Color) = (0.9914412,1,0.709,1)
        _PhaseSpeedMult ("Phase Speed Mult", Range(-1, 0.0001)) = 0
        _WaveStrengthB ("Wave Strength B", Range(0, 1)) = 0
        _WaveStrengthA ("Wave Strength A", Range(0, 1)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Overlay+1"
            "RenderType"="Overlay"
            "PreviewType"="Plane"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            ZTest Always
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float _FreqA;
            uniform float _XA;
            uniform float _YA;
            uniform float _XB;
            uniform float _YB;
            uniform float _FreqB;
            uniform float _Wavesinputmin;
            uniform float _Wavesinputmax;
            uniform float _Wavesoutputmin;
            uniform float _Wavesoutputmax;
            uniform float _Glowremapinmin;
            uniform float _Glowremapinmax;
            uniform float _Glowremapoutmin;
            uniform float _Glowremapoutmax;
            uniform float4 _ColorA;
            uniform float4 _ColorB;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _DamageZoneMult;
            uniform float4 _DamageZoneTintStart;
            uniform float4 _DamageZoneTintEnd;
            uniform float _PhaseSpeedMult;
            uniform float _WaveStrengthA;
            uniform float _WaveStrengthB;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 projPos : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float2 sceneUVs = (i.projPos.xy / i.projPos.w);
////// Lighting:
////// Emissive:
                float4 node_7218 = _Time;
                float node_5387 = (node_7218.g*_PhaseSpeedMult);
                float node_5783 = saturate((_Wavesoutputmin + ( (sin(((_FreqA*distance(float2(_XA,_YA),float2((sceneUVs.x * 2 - 1)*(_ScreenParams.r/_ScreenParams.g), sceneUVs.y * 2 - 1).rg))+node_5387)) - _Wavesinputmin) * (_Wavesoutputmax - _Wavesoutputmin) ) / (_Wavesinputmax - _Wavesinputmin)));
                float node_7609 = saturate((_Wavesoutputmin + ( (sin((node_5387+(_FreqB*distance(float2(_XB,_YB),float2((sceneUVs.x * 2 - 1)*(_ScreenParams.r/_ScreenParams.g), sceneUVs.y * 2 - 1).rg)))) - _Wavesinputmin) * (_Wavesoutputmax - _Wavesoutputmin) ) / (_Wavesinputmax - _Wavesinputmin)));
                float node_9584 = (_Glowremapoutmin + ( (saturate((node_5783*node_7609)) - _Glowremapinmin) * (_Glowremapoutmax - _Glowremapoutmin) ) / (_Glowremapinmax - _Glowremapinmin));
                float3 node_2169 = saturate(max((_DamageZoneMult*lerp(_DamageZoneTintStart.rgb,_DamageZoneTintEnd.rgb,float3(node_9584,node_9584,node_9584))),((_ColorA.rgb*node_5783*_WaveStrengthA)+(node_7609*_ColorB.rgb*_WaveStrengthB))));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 emissive = (node_2169+_MainTex_var.rgb);
                float3 finalColor = emissive;
                return fixed4(finalColor,node_9584);
            }
            ENDCG
        }
    }
    CustomEditor "ShaderForgeMaterialInspector"
}
