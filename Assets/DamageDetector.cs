﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageDetector : MonoBehaviour
{
    public delegate void DamageEvent (DamageDetector sender);
    public event DamageEvent EnemyDestroyed;
    public GameObject explosionPrefab;

    public const float DamageMult = 4;
    public const float MaxParticlesOverTime = 100;

    public Transform healthBar;
    public float healthBarScale = 2;
    public float maxHealth = 100;

    public bool isHealthPack = false;

    private Camera cam;
    private RadialsImageEffect radials;
    private Text debugText;
    public bool vulnerable = true;
    public float sy;
    private float health;
    private ParticleSystem particles;
    private ParticleSystem.EmissionModule emission;

	void Start ()
    {
        health = maxHealth;
        cam = Camera.main;
        radials = cam.GetComponent<RadialsImageEffect>();
        debugText = GetComponentInChildren<Text>(true);
        particles = GetComponentInChildren<ParticleSystem>(true);
        var emission = particles.emission;
        emission.rateOverTime = 0;
	}
	
	void Update ()
    {
        var screenPos = cam.WorldToScreenPoint(transform.position);
        vulnerable = screenPos.y > -20 && screenPos.y < Screen.height - 15;
        sy = screenPos.y;

        if (sy < -20)
        {
            // deal damage to the player if it goes past the bottom edge of the screen
            if (!isHealthPack)
            {
                Health.instance.UpdateHealth(-15);
            }

            Destroy(gameObject);
        }
        else
        {
            radials.RegisterEnemy(this);
        }
	}

    public void TakeDamage(Color pixel)
    {
        var damage = pixel.a;
        debugText.text = damage + "";
        health = Mathf.Max(0, health - damage * DamageMult);

        var sc = healthBar.transform.localScale;
        healthBar.transform.localScale = new Vector3(healthBarScale * (health / maxHealth), sc.y, sc.z);

        var emission = particles.emission;
        emission.rateOverTime = Mathf.Clamp01(2 * damage) * MaxParticlesOverTime;

        if (health < (maxHealth * 0.05f))
        {
            var explosion = Instantiate(explosionPrefab);
            explosion.transform.position = transform.position;
            if (EnemyDestroyed != null) EnemyDestroyed.Invoke(this);
            Destroy(gameObject);
        }
    }
}
