﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public static Health instance;
    public HealthBar healthBar;

    public float health { get; private set; }

    void Awake()
    {
        instance = this;
        health = 100;
    }

	public void UpdateHealth (float diff)
    {
        health = Mathf.Clamp(health + diff, 0, 100);
        healthBar.currentHealth = health;
	}

	void Update ()
    {
        if (health == 0)
        {
            Debug.Log("GAME OVER");
        }
	}
}
