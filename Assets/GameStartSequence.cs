﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStartSequence : MonoBehaviour
{
    private RadialsImageEffect radials;

    IEnumerator Start()
    {
        radials = Camera.main.GetComponent<RadialsImageEffect>();

        radials.waveStrengthA = 0;
        radials.waveStrengthB = 0;
        radials.damageZoneStrength = 0;
        Health.instance.healthBar.gameObject.SetActive(false);

        yield return new WaitForSeconds(0.5f);

        while (radials.waveStrengthA < 1)
        {
            yield return new WaitForSeconds(0.01f);
            radials.waveStrengthA = Mathf.Clamp01(radials.waveStrengthA + 0.015f);
        }

        yield return new WaitForSeconds(1);

        while (radials.waveStrengthB < 1)
        {
            yield return new WaitForSeconds(0.01f);
            radials.waveStrengthB = Mathf.Clamp01(radials.waveStrengthB + 0.015f);
            radials.damageZoneStrength = radials.waveStrengthB;
        }

        yield return new WaitForSeconds(0.3f);

        Health.instance.healthBar.gameObject.SetActive(true);
    }
	
	void Update ()
    {
		
	}
}
